' V2.1.1
' 07.06.2020
' (c) Torben Eims
' See https://gitlab.com/torbeneims/schulbuchausleihe-buchreihen

Set WshShell = WScript.CreateObject("WScript.Shell")
doContinue = True
iterationDelay = 100
input = inputbox( _
	"1) Barcodes eingeben (Die L"+chr(228)+"nge beider Nummern muss "+chr(252)+"bereinstimmen, ggf. mit 0 auff"+chr(252)+"llen)" + vbnewline + _
	"2) Eingabefeld anklicken" + vbnewline + _
	"3) Das Programm gibt die alle dazwischen liegenden Nummer sowie die Grenzen ein. Die L"+chr(228)+"nge stimmt mit der Eingabe "+chr(252)+"berein, ggf. wird mit 0 aufgef"+chr(252)+"llt." + vbnewline + vbnewline + _
	"Ggf. Verz"+chr(246)+"gerung zwischen den Eingaben in ms eingeben (Standard " + CStr(iterationDelay) + ")")
if IsEmpty(input) then WScript.Quit
if input <> "" then iterationDelay = CInt(input)

Do While doContinue: Do 'null loop for logic short-circuit
	
	input = inputbox("Erster Barcode (mit f"+chr(252)+"hrenden Nullen)")
	barcodeLen = Len(input)
	first = CInt(input)
	if (first = 0) then WScript.Quit
	
	input = inputbox("Letzter Barcode (mit f"+chr(252)+"hrenden Nullen)")
	if barcodeLen <> Len(input) then 
		msgbox("Die L"+chr(228)+"ngen der Barcodes stimmen nicht "+chr(252)+"berein")
		Exit Do
	end if
	last = CInt(input)
	if (last = 0) then WScript.Quit
	
	WScript.Sleep 3000

	for current = first To last
		currentString = CStr(current)
		WshShell.SendKeys (String(barcodeLen - len(currentString),"0") & currentString)
		WshShell.SendKeys "{ENTER}"
		WScript.Sleep iterationDelay
	Next
	doContinue = (msgbox("Eine weitere Buchreihe erstellen?", vbYesNo) = vbYes)
	
	Loop While False ' end of null loop
Loop